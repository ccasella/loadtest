from locust import HttpLocust, TaskSet, task, between

def index(l):
    l.client.get("/")

class UserTasks(TaskSet):
    # one can specify tasks like this
    tasks = [index]
    
class WebsiteUser(HttpLocust):
    """
    Locust user class that does requests to the locust web server running on localhost
    """
    host = "http://127.0.0.1:8089"
    wait_time = between(2, 5)
    task_set = UserTasks
